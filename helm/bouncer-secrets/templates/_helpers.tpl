{{- define "bouncer.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "bouncer.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "bouncer.labels" -}}
helm.sh/chart: {{ include "bouncer.chart" . }}
{{ include "bouncer.selectorLabels" . }}
{{- end -}}

{{- define "bouncer.selectorLabels" -}}
app.kubernetes.io/name: {{ include "bouncer.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
