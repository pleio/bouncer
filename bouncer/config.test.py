import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'UftdWWEVwcpJ9g3sfF7U'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# OIDC: this is example configuration that matches dex.yaml (for local testing)
OIDC_RP_CLIENT_ID = 'bouncer'
OIDC_RP_CLIENT_SECRET = 'ZXhhbXBsZS1hcHAtc2VjcmV0'
OIDC_OP_AUTHORIZATION_ENDPOINT = 'http://localhost:5556/auth'
OIDC_OP_TOKEN_ENDPOINT = 'http://localhost:5556/token'
OIDC_OP_USER_ENDPOINT = 'http://localhost:5556/userinfo'
OIDC_OP_JWKS_ENDPOINT = 'http://localhost:5556/keys'

# The topic that we are following on the SNS queue
SNS_TOPIC_ARN = 'arn:aws:sns:eu-west-1:233403374923:test'

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
