# Bouncer
Bouncer receives e-mail bounce messages from a SNS topic and saves the invalid e-mail addresses in a local database.

## Setup a development environment

```
cp .env-example .env
```

Update the SNS_TOPIC_ARN for testing

Start the docker containers

```
docker-compose build
docker-compose up
```

## First login

You can login to the admin portal at https://localhost:8000/admin/ with username `admin@example.com` and password `password`. After login you need to assign staff and superuser permissions to your account with:

```bash
docker-compose exec bouncer bash
python3 manage.py shell
>>> from core.models import User
>>> user = User.objects.get(email='admin@example.com')
>>> user.is_staff =  True
>>> user.is_superuser = True
>>> user.save()
>>> quit()
```

Now you can access the admin panel.
