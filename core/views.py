import html
import json
import logging

import requests
from django.conf import settings
from django.db import IntegrityError
from django.db.models import F
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Message, Orphan
from .serializers import OrphanSerializer
from .validators import SignatureValidator, ValidationError

logger = logging.getLogger('django')
validator = SignatureValidator(settings.SNS_TOPIC_ARN)


def index(request):
    return HttpResponse('OK')


@require_http_methods(['POST'])
@csrf_exempt
def sns_receive(request):
    try:
        payload = json.loads(request.body.decode('utf-8'))
    except json.decoder.JSONDecodeError:
        logger.info('Received an invalid JSON request')
        return HttpResponseBadRequest()

    try:
        validator.validate(payload)
    except ValidationError as e:
        logger.info(
            'A validation error occured when validating SNS request: %s', e)
        return HttpResponseBadRequest()

    message_type = payload.get('Type')

    if message_type == 'SubscriptionConfirmation':
        return verify_subscription(payload)

    if message_type == 'Notification':
        return process_notification(payload)

    logger.info('Unknown message type %s', message_type)

    return JsonResponse({})


def verify_subscription(payload):
    subscribe_url = payload.get('SubscribeURL')

    if not subscribe_url:
        return HttpResponseBadRequest()

    response = requests.get(subscribe_url)
    if response.status_code != 200:
        logger.error('Failed to verify SNS subscription')

    return JsonResponse({})


def process_notification(payload):
    message = payload.get('Message')

    if not message:
        logger.info('Message is empty')
        return HttpResponseBadRequest()

    try:
        message = json.loads(message)
    except json.decoder.JSONDecodeError:
        logger.info('Received an invalid JSON message')
        return HttpResponseBadRequest()

    notificationType = message.get('notificationType')
    if notificationType != 'Bounce':
        logger.info('Received an unknown notification type %s',
                    notificationType)
        return JsonResponse({})

    bounce = message.get('bounce')
    if bounce.get('bounceType') != 'Permanent':
        logger.info('This bounceType is not permanent, skipping')
        return JsonResponse({})

    mail = message.get('mail')

    for recipient in bounce.get('bouncedRecipients'):
        try:
            orphan, created = Orphan.objects.get_or_create(
                email=recipient['emailAddress'],
                defaults={
                    'action': recipient['action'],
                    'status': recipient['status'],
                    'diagnostic_code': recipient['diagnosticCode'],
                }
            )

            if not created:
                orphan.count = F('count') + 1
                orphan.save()
        except IntegrityError:
            pass

        Message.objects.create(
            orphan=orphan,
            message_id=html.unescape(mail['commonHeaders']['messageId']),
            subject=html.unescape(mail['commonHeaders']['subject']),
        )

    return JsonResponse({})


class OrphanList(APIView):
    LIMIT = 500

    ALLOWED_FILTERS = [
        'last_received',
        'last_received__gt',
        'last_received__lt'
    ]

    def get(self, request, api_format=None):
        resultset = Orphan.objects.order_by('last_received')

        for allowed_filter in self.ALLOWED_FILTERS:
            if request.GET.get(allowed_filter):
                resultset = resultset.filter(
                    **{allowed_filter: request.GET.get(allowed_filter)})

        serializer = OrphanSerializer(resultset[:self.LIMIT], many=True)
        return Response(serializer.data)
