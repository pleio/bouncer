from rest_framework import permissions
from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class OIDCAuthBackend(OIDCAuthenticationBackend):
    def create_user(self, claims):
        user = super(OIDCAuthBackend, self).create_user(claims)
        user.username = claims.get('email')
        user.first_name = claims.get('given_name', '')
        user.last_name = claims.get('family_name', '')
        user.save()

        return user

    def update_user(self, user, claims):
        user.username = claims.get('email')
        user.first_name = claims.get('given_name', '')
        user.last_name = claims.get('family_name', '')
        user.save()

        return user


class HasApiAccessPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False

        if request.user.has_api_access:
            return True

        return False
