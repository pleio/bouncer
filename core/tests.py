import json
from django.conf import settings
from django.test import TestCase
from django.urls import reverse

mock_subscription_confirmation = {
    "Type" : "SubscriptionConfirmation",
    "MessageId" : "f9370cc3-84f7-43d9-8fff-9cef8480d315",
    "Token" : "2336412f37fb687f5d51e6e2425dacbba63549977c733285270e2844670cf893ff6cfcb0b1075b1263ad05045b4084681955b28a5d33b69074512844755b23319805c589e2c9efcf716e7764fc3e133f3a145ef4ee7e2cc7aacb66d2ab09919012732a12de42f200544fd45ee47deaed",
    "TopicArn" : "arn:aws:sns:eu-west-1:233403374923:test",
    "Message" : "You have chosen to subscribe to the topic arn:aws:sns:eu-west-1:233403374923:test.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
    "SubscribeURL" : "https://sns.eu-west-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:eu-west-1:233403374923:test&Token=2336412f37fb687f5d51e6e2425dacbba63549977c733285270e2844670cf893ff6cfcb0b1075b1263ad05045b4084681955b28a5d33b69074512844755b23319805c589e2c9efcf716e7764fc3e133f3a145ef4ee7e2cc7aacb66d2ab09919012732a12de42f200544fd45ee47deaed",
    "Timestamp" : "2022-09-01T20:14:54.073Z",
    "SignatureVersion" : "1",
    "Signature" : "Vgs/n7ozMmCQV0DKTsC7i21cPJW8ijRP35XCP1TJPjeVF2PolOy+oX5y/Q2KtU91N0ZifeAcn5jWhskwciudHqtor/M4WiHcdT9BstZVIrCIApNnCwsVeb6bEJ5OgFYUBCSVvETWTFQhvTT/CAOztWkYk9+tRPEiUdEEH7kkl0tkCdT82LdUjHQ4Lt0uXkyy2bealhk/dEXS213TCHCGmaeXVwqsUXiK82IBtMePdcXbB7v6OEBY5T3cRd55Wk1JpA9Hbp+Cyrd2lnwJgZMnU/+9sobGGJVFOT6QcS+bjAxU/6D+5weVk0TJBLZStSCLDm+XRkO9LB3TxUbi6wzxoA==",
    "SigningCertURL" : "https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-56e67fcb41f6fec09b0196692625d385.pem"
}

mock_notification = {
    "Type" : "Notification",
    "MessageId" : "4a3802a5-602e-5f39-b0e0-592e754121e1",
    "TopicArn" : "arn:aws:sns:eu-west-1:233403374923:test",
    "Subject" : "Test",
    "Message" : "{\n  \"test\": true\n}",
    "Timestamp" : "2022-09-01T20:16:47.559Z",
    "SignatureVersion" : "1",
    "Signature" : "BTMUczOclcdqt07xGm1FaV/LfChJQHpAGmG9QhjkNKJovyA/nBPYhAcT2Atx21zz0LU/wy61+EdeQQrE5U3WqQ9Y0C0gkmBcAYOwo1ejiN0APj5JWgjEvMTvFK7WOsXGd24ZTE4EmpD9TrT3MY/XowvQqoqbyQEUl9poGFoSX+jlwERavRWpyFnT59G4G+7QZwzrgWli/03/W2yWhSX5ArmCbcGe/OrGNesRH39lB2+JJopjU4Bi7h6F1RaGHDMeo3kSprQGZnGzKBjFNLT1y6zO5qbXk0EEE5NqTADm3tHdppS7FiBOorXtFEHKmX40PDeXqF/A4+lWQbRfMW6FLg==",
    "SigningCertURL" : "https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-56e67fcb41f6fec09b0196692625d385.pem",
    "UnsubscribeURL" : "https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:233403374923:test:bd5e937f-d2b1-49ac-a0eb-f33066e40a84"
}

mock_invalid_notification = {
    "Type" : "Notification",
    "MessageId" : "4a3802a5-602e-5f39-b0e0-592e754121e1",
    "TopicArn" : "arn:aws:sns:eu-west-1:233403374923:test",
    "Subject" : "Test",
    "Message" : "123",
    "Timestamp" : "2022-09-01T20:16:47.559Z",
    "SignatureVersion" : "1",
    "Signature" : "BTMUczOclcdqt07xGm1FaV/LfChJQHpAGmG9QhjkNKJovyA/nBPYhAcT2Atx21zz0LU/wy61+EdeQQrE5U3WqQ9Y0C0gkmBcAYOwo1ejiN0APj5JWgjEvMTvFK7WOsXGd24ZTE4EmpD9TrT3MY/XowvQqoqbyQEUl9poGFoSX+jlwERavRWpyFnT59G4G+7QZwzrgWli/03/W2yWhSX5ArmCbcGe/OrGNesRH39lB2+JJopjU4Bi7h6F1RaGHDMeo3kSprQGZnGzKBjFNLT1y6zO5qbXk0EEE5NqTADm3tHdppS7FiBOorXtFEHKmX40PDeXqF/A4+lWQbRfMW6FLg==",
    "SigningCertURL" : "https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-56e67fcb41f6fec09b0196692625d385.pem",
    "UnsubscribeURL" : "https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:233403374923:test:bd5e937f-d2b1-49ac-a0eb-f33066e40a84"
}


class SNSReceiveViewTests(TestCase):
    def setUp(self):
        settings.SNS_TOPIC_ARN = 'arn:aws:sns:eu-west-1:462493257891:test'

    def test_valid_subscription(self):
        response = self.client.post(
            reverse('sns_receive'), json.dumps(
                mock_subscription_confirmation), content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_invalid_message(self):
        response = self.client.post(
            reverse('sns_receive'), json.dumps(mock_invalid_notification), content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_valid_message(self):
        response = self.client.post(
            reverse('sns_receive'), json.dumps(mock_notification), content_type='application/json')
        self.assertEqual(response.status_code, 200)
