from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    has_api_access = models.BooleanField(default=False)

    def has_perm(self, perm, obj=None):
        # pylint: disable=unused-argument
        return True

    def has_module_perms(self, app_label):
        # pylint: disable=unused-argument
        return True


class Orphan(models.Model):
    email = models.EmailField(max_length=254, unique=True, db_index=True)
    action = models.CharField(max_length=254, null=True)
    status = models.CharField(max_length=254, null=True)
    diagnostic_code = models.CharField(max_length=254, null=True)
    last_received = models.DateTimeField(auto_now=True, db_index=True)
    count = models.IntegerField(default=1)

    def __str__(self):
        return self.email


class Message(models.Model):
    orphan = models.ForeignKey('Orphan', on_delete=models.CASCADE)
    message_id = models.CharField(max_length=254)
    subject = models.CharField(max_length=254)

    def __str__(self):
        return self.message_id
