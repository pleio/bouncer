from rest_framework import serializers
from .models import Orphan


class OrphanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orphan
        fields = ['email', 'last_received', 'count']
