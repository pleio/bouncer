import base64
import requests
import re
from functools import lru_cache
from cryptography import x509
from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import hashes

CERTIFICATE_URL_REGEX = r"^https://sns\.[-a-z0-9]+\.amazonaws\.com?/"


class ValidationError(Exception):
    pass


class SignatureValidator():
    def __init__(self, topic_arn):
        self.topic_arn = topic_arn

    def validate(self, message):
        signature_version = message.get('SignatureVersion')
        if signature_version != '1':
            raise ValidationError(
                'Unexpected SignatureVersion {}'.format(signature_version))

        topic_arn = message.get('TopicArn')
        if topic_arn != self.topic_arn:
            raise ValidationError(
                'Received TopicArn {}, expected {}'.format(topic_arn, self.topic_arn))

        cert_url = message.get('SigningCertURL')
        if not re.search(CERTIFICATE_URL_REGEX, cert_url):
            raise ValidationError(
                'The SigningCertURL {} appears not te be from Amazon'.format(cert_url))

        # TODO: We should verify Timestamp as well in order to limit chance replay attacks, but this is very unlikely in our case
        # where all messages are sent by Amazon over SSL.

        signing_cert = self._download_certificate(cert_url)
        cert = x509.load_pem_x509_certificate(
            signing_cert, default_backend())

        public_key = cert.public_key()

        signature = base64.b64decode(message.get('Signature'))
        content = self._get_content(message).encode()

        try:
            return public_key.verify(
                signature,
                content,
                padding.PKCS1v15(),
                hashes.SHA1()
            )
        except InvalidSignature:
            raise ValidationError('Invalid signature')

    @lru_cache(maxsize=10)
    def _download_certificate(self, cert_url):
        try:
            response = requests.get(cert_url)
        except requests.exceptions.RequestException:
            raise ValidationError(
                'Could not fetch signing certificate from {}'.format(cert_url))

        return response.content

    def _get_content(self, message):
        lines = []
        for key in self._get_keys(message):
            if key not in message:
                raise ValidationError('Missing {} key'.format(key))

            lines.append(key)
            lines.append(message[key])

        return '\n'.join(lines) + '\n'

    def _get_keys(self, message):
        message_type = message.get('Type')

        if message_type == 'Notification':
            if message.get('Subject'):
                return ['Message', 'MessageId', 'Subject', 'Timestamp', 'TopicArn', 'Type']

            return ['Message', 'MessageId', 'Timestamp', 'TopicArn', 'Type']

        if message_type in ['SubscriptionConfirmation', 'UnsubscribeConfirmation']:
            return ['Message', 'MessageId', 'SubscribeURL', 'Timestamp', 'Token', 'TopicArn', 'Type']

        raise ValidationError('Unknown message type {}'.format(message_type))
