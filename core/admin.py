from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

from .models import Message, Orphan, User


class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'has_api_access'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_filter = ('is_superuser', 'is_active', 'groups')
    list_display = ('username', 'email', 'first_name',
                    'last_name', 'is_staff', 'is_superuser', 'has_api_access')


class MessageInline(admin.TabularInline):
    model = Message
    ordering = ['-pk']
    readonly_fields = ['message_id', 'subject']


class OrphanAdmin(admin.ModelAdmin):
    ordering = ['-pk']
    list_display = ['email', 'last_received', 'count']
    readonly_fields = ['email', 'action', 'status', 'diagnostic_code', 'count']
    search_fields = ('email', )
    inlines = [
        MessageInline
    ]


class MessageAdmin(admin.ModelAdmin):
    ordering = ['-pk']
    list_display = ('message_id', 'subject')


admin.site.register(User, UserAdmin)
admin.site.register(Orphan, OrphanAdmin)
admin.site.register(Message, MessageAdmin)
